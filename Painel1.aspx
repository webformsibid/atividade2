﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Painel1.aspx.cs" Inherits="atividade02.Painel1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif">
    <h1 style="text-align:center">Panel - Atividade</h1>
    <form id="form1" runat="server">
            <table style="width: 100%;" class="tbInformacoesPessoais">
                <tr>
                    <td>
                        <asp:Panel ID="Panel" runat="server">
                            <asp:Panel ID="Panel1" runat="server">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2">
                                            <strong>Informações Pessoais</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nome</td>
                                        <td><asp:TextBox ID="txtbNome" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Sobrenome</td>
                                        <td><asp:TextBox ID="txtbSobrenome" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Gênero</td>
                                        <td><asp:TextBox ID="txtbGenero" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Celular</td>
                                        <td><asp:TextBox ID="txtbCelular" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px; height:26px "></td>
                                        <td><asp:Button ID="btn1proximo" runat="server" Text="Proximo" OnClick="btn1proximo_Click" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Panel2" runat="server">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2">
                                            <strong>Endereço</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Endereço</td>
                                        <td><asp:TextBox ID="txtbEndereço" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Cidade</td>
                                        <td><asp:TextBox ID="txtbCidade" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>CEP</td>
                                        <td><asp:TextBox ID="txtbCEP" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr >
                                        <td style="width:100px; height:26px "></td>
                                        <td><asp:Button ID="btn1Voltar" runat="server" Text="Voltar" OnClick="btn1Voltar_Click" /></tdstyle="width:100px;>
                                    </tr>
                                    <tr >
                                        <td style="width:100px; height:26px "></td>
                                        <td><asp:Button ID="btn2Proximo" runat="server" Text="Proximo" OnClick="btn2Proximo_Click"  /></tdstyle="width:100px;>
                                    </tr>
                                </table>
                            </asp:Panel>

                              <asp:Panel ID="Panel3" runat="server">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2">
                                            <strong>Efetuar Login</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Login</td>
                                        <td><asp:TextBox ID="txtbLogin" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Senha</td>
                                        <td><asp:TextBox ID="txtbSenha" runat="server" TextMode="Password"></asp:TextBox></td>
                                    </tr>
                                    <tr >
                                        <td style="width:100px; height:26px "></td>
                                        <td><asp:Button ID="btn2Voltar" runat="server" Text="Voltar" OnClick="btn2Voltar_Click" /></tdstyle="width:100px;>
                                            <asp:Button ID="BtnEnviar" runat="server" OnClick="BtnEnviar_Click" style="margin-left: 6px" Text="Enviar" />
                                    </tr>
                                    <tr >
                                        <td style="width:100px; height:26px "></td>
                                        <td></tdstyle="width:100px;>
                                    </tr>
                                    <tr >
                                        <td style="width:100px; height:26px "></td>
                                        <td>
                                            <asp:Label ID="lblDadosSalvos" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>

                
            </table>
        <div>
        </div>
    </form>
</body>
</html>
