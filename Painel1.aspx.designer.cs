﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Esse código foi gerado por uma ferramenta.
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for recriado
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace atividade02
{


    public partial class Painel1
    {

        /// <summary>
        /// Controle form1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// Controle Panel.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel;

        /// <summary>
        /// Controle Panel1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel1;

        /// <summary>
        /// Controle txtbNome.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbNome;

        /// <summary>
        /// Controle txtbSobrenome.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbSobrenome;

        /// <summary>
        /// Controle txtbGenero.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbGenero;

        /// <summary>
        /// Controle txtbCelular.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbCelular;

        /// <summary>
        /// Controle btn1proximo.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn1proximo;

        /// <summary>
        /// Controle Panel2.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel2;

        /// <summary>
        /// Controle txtbEndereço.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbEndereço;

        /// <summary>
        /// Controle txtbCidade.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbCidade;

        /// <summary>
        /// Controle txtbCEP.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbCEP;

        /// <summary>
        /// Controle btn1Voltar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn1Voltar;

        /// <summary>
        /// Controle btn2Proximo.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn2Proximo;

        /// <summary>
        /// Controle Panel3.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel3;

        /// <summary>
        /// Controle txtbLogin.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbLogin;

        /// <summary>
        /// Controle txtbSenha.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtbSenha;

        /// <summary>
        /// Controle btn2Voltar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn2Voltar;

        /// <summary>
        /// Controle BtnEnviar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button BtnEnviar;

        /// <summary>
        /// Controle lblDadosSalvos.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblDadosSalvos;
    }
}
